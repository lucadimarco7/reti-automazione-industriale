# Progetto Automotive
###### _Autori: Luca Dimarco - Antonino Salemi_
###### _Anno di realizzazione: 2022_

## Contenuto
Nel repository è contenuto un progetto relativo all'ambito automotive, i dettagli del progetto sono esposti nel file `Relazione.pdf`.
Il progetto è stato realizzato in linguaggio C++.