#ifndef __SIMPLEAUTOMOTIVE_ETHNIC_H_
#define __SIMPLEAUTOMOTIVE_ETHNIC_H_

#include <omnetpp.h>

using namespace omnetpp;

class EthNIC: public cSimpleModule {
protected:
    simsignal_t flrSwitch;
    //per gli end-node mettiamo un segnale per ogni flusso, vettore di segnali
    std::vector<simsignal_t> flr;
    std::vector<simsignal_t> queueSig;
    simsignal_t queue;
    simsignal_t deadlineSig;

    typedef enum {
        TX_STATE_IDLE, TX_STATE_TRANSMITTING, TX_STATE_WAIT_IFG
    } ethstate_t;

    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void transmit();

    ethstate_t txstate;

    bool promMode;
    double datarate;
    int vectorSize;
    double droppedFrameSwitch;
    double totalFrameSwitch;
    std::vector<double> droppedFrame;
    std::vector<double> totalFrame;
    bool inSwitch;
    cPacketQueue txqueue;
};

#endif
