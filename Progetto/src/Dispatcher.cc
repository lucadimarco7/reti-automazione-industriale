#include "Dispatcher.h"
#include "Packets_m.h"
#include "EthFrames_m.h"

Define_Module(Dispatcher);

void Dispatcher::initialize() {

}

void Dispatcher::handleMessage(cMessage *msg) {
    Packet *pkt = check_and_cast<Packet*>(msg);
    if (strcmp(msg->getArrivalGate()->getName(), "upperLayerIn") == 0) {
        send(msg, "lowerLayerOut");
    } else if (strcmp(msg->getArrivalGate()->getName(), "lowerLayerIn") == 0) {
        for (int i = 0; i < msg->getSenderModule()->getVectorSize(); i++)
            send(msg, "upperLayerOut", i);
    }
}
