#include "App.h"
#include "Packets_m.h"

Define_Module(App);

void App::initialize() {
    packetName = par("packetName");
    period = par("period");
    payloadSize = par("payloadSize");
    burstSize = par("burstSize");
    startTime = par("startTime");
    deadlineR = par("deadlineR");
    source = par("source").stringValue(); //questo per far si che restituisca un puntatore a char costante
    destination = par("destination").stringValue();

    //avviamo la macchina statica
    if (startTime > 0) {
        cMessage *txtimer = new cMessage("TxTimer");
        scheduleAt(startTime, txtimer);
    }
}

void App::handleMessage(cMessage *msg) {
    if (msg->isSelfMessage()) {
        if (strcmp(msg->getName(), "TxTimer") == 0) {
            startTransmission();
            if (period != 0) {
                scheduleAt(simTime() + period, msg);
            }
            return;
        }
    }

    ControlInfo *ci = check_and_cast<ControlInfo*>(msg->removeControlInfo());

    if (strcmp(ci->getDst(), source) != 0) {
        EV << "destinazione: " << ci->getDst() << " source: " << source
                  << "  Destinazione non corretta: dropping..." << endl;
        delete msg;
        delete ci;
        return;
    }

    Packet *pkt = check_and_cast<Packet*>(msg);
    const char *messaggio = pkt->getName();

    EV << "E' arrivato un messaggio " << messaggio << " flusso: "
              << pkt->getSrcProcId() << " dimensione del payload: "
              << pkt->getByteLength() << " Byte" << endl;
    delete msg;
    delete ci;
}

void App::startTransmission() {
    Packet *pkt;
    ControlInfo *ci;
    for (int i = 0; i < burstSize; i++) {
        pkt = new Packet();
        pkt->setName(packetName);
        pkt->setDeadlineR(deadlineR);
        pkt->setSrcProcId(this->getIndex());
        pkt->setByteLength(payloadSize);
        pkt->setGenTime(simTime());
        if (i == (burstSize - 1)) {
            pkt->setLastBurstPacket(true);
        }

        ci = new ControlInfo();
        ci->setSrc(source);
        ci->setDst(destination);

        pkt->setControlInfo(ci);

        send(pkt, "lowerLayerOut");
    }

}
