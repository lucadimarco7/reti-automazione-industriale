#include "RelayUnit.h"
#include "EthFrames_m.h"

Define_Module(RelayUnit);

void RelayUnit::initialize() {
    port_count = gateSize("lowerLayerIn");
}

void RelayUnit::handleMessage(cMessage *msg) {
    int idx;
    int port_idx;

    if (!msg->isSelfMessage()) {
        idx = msg->getArrivalGate()->getIndex();
        EthFrame *frame = check_and_cast<EthFrame*>(msg);

        port_idx = getPortFromAdd(frame->getDst());

        if (port_idx != -1) {
            send(frame->dup(), "lowerLayerOut", port_idx);
        } else if (port_idx == -1) {
            for (int i = 0; i < port_count; i++) {
                if (i != idx) {
                    send(frame->dup(), "lowerLayerOut", i);
                }
            }
        }
        port_idx = getPortFromAdd(frame->getSrc());
        if (port_idx == -1) {
            TableEntry *entry = new TableEntry;
            strncpy(entry->mac_addr, frame->getSrc(), 10);
            entry->eth_port_idx = idx;
            fw_table.push_back(entry);
        }
        delete frame;
    }
}

int RelayUnit::getPortFromAdd(const char *mac) {
    for (int i = 0; i < fw_table.size(); i++) {
        if (strcmp(fw_table[i]->mac_addr, mac) == 0) {
            return fw_table[i]->eth_port_idx;
        }
    }
    return -1;
}
