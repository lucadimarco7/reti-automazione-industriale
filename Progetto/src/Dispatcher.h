#ifndef __SIMPLEAUTOMOTIVEMODIFIED_DISPATCHER_H_
#define __SIMPLEAUTOMOTIVEMODIFIED_DISPATCHER_H_

#include <omnetpp.h>

using namespace omnetpp;

class Dispatcher: public cSimpleModule {
protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

};

#endif
