#ifndef __SIMPLEAUTOMOTIVEMODIFIED_RELAYUNIT_H_
#define __SIMPLEAUTOMOTIVEMODIFIED_RELAYUNIT_H_

#include <omnetpp.h>

using namespace omnetpp;

class RelayUnit: public cSimpleModule {
protected:
    typedef struct {
        char mac_addr[10];
        int eth_port_idx;
    } TableEntry;

    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    virtual int getPortFromAdd(const char *mac);

    std::vector<TableEntry*> fw_table;
    int port_count;
};

#endif
