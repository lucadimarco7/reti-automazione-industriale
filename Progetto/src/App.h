#ifndef __SIMPLEAUTOMOTIVE_LDBURST_H_
#define __SIMPLEAUTOMOTIVE_LDBURST_H_

#include <omnetpp.h>

using namespace omnetpp;

class App: public cSimpleModule {
private:
protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);

    virtual void startTransmission();

    simtime_t period;
    simtime_t startTime;
    simtime_t deadlineR;
    const char *packetName;
    int payloadSize;
    const char *source;
    const char *destination;
    int burstSize;

};

#endif
