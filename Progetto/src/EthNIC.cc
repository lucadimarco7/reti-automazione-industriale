#include "EthNIC.h"
#include "EthFrames_m.h"
#include "Packets_m.h"

Define_Module(EthNIC);

void EthNIC::initialize() {
    //definizione dei segnali, quelli per gli end-node sono vettori di segnali, uno per ogni flusso
    //quindi si ridimensionano in base a vectorSize, che indica il numero di app del nodo
    flrSwitch = registerSignal("flrSwitch");
    queue = registerSignal("queue");
    vectorSize = par("vectorSize");
    flr.resize(vectorSize);
    queueSig.resize(vectorSize);
    droppedFrame.resize(vectorSize);
    totalFrame.resize(vectorSize);
    droppedFrameSwitch=0;
    totalFrameSwitch=0;
    for (int i = 0; i < vectorSize; i++) {
        char signalName[32];
        sprintf(signalName, "flr flusso: %d", i);
        simsignal_t signal = registerSignal(signalName);
        cProperty *statistic = getProperties()->get("statistic", "flrTemplate");
        getEnvir()->addResultRecorders(this, signal, signalName,  statistic);
        flr[i] = signal;
    }
    for (int i = 0; i < vectorSize; i++) {
            char signalName[32];
            sprintf(signalName, "coda flusso: %d", i);
            simsignal_t signal = registerSignal(signalName);
            cProperty *statisticQueue = getProperties()->get("statisticQueue", "queueTemplate");
            getEnvir()->addResultRecorders(this, signal, signalName,  statisticQueue);
            queueSig[i] = signal;
        }
    deadlineSig = registerSignal("deadlineSig");
    datarate = par("datarate");
    inSwitch = par("inSwitch");
    promMode = par("promMode");
    txstate = TX_STATE_IDLE;
}

void EthNIC::handleMessage(cMessage *msg) {
    if (msg->isSelfMessage()) {
        if (strcmp(msg->getName(), "TxEndTimer") == 0) {
            assert(txstate == TX_STATE_TRANSMITTING);
            delete msg;
            txstate = TX_STATE_WAIT_IFG;
            double t = 96.0 / datarate;
            cMessage *ifgtim = new cMessage("IFGEndTimer");
            scheduleAt(simTime() + t, ifgtim);
            return;
        } else if (strcmp(msg->getName(), "IFGEndTimer") == 0) {
            assert(txstate == TX_STATE_WAIT_IFG);
            delete msg;
            txstate = TX_STATE_IDLE;
            transmit();
            return;
        } else if (strcmp(msg->getName(), "RxEndTimer") == 0) {
            cPacket *pkt = dynamic_cast<cPacket*>(msg->removeControlInfo());
            delete msg;
            send(pkt, "upperLayerOut", 0);
            return;
        }
    }

    //se viene dall'app
    if (strcmp(msg->getArrivalGate()->getName(), "upperLayerIn") == 0) {

        int flusso=msg->getSrcProcId(); //serve per discriminare i flussi, capire da che flusso arriva
        double tmp=totalFrame[flusso]+1;

        if (inSwitch) {
            totalFrameSwitch++;
            cPacket *pkt = check_and_cast<cPacket*>(msg);
            if (txqueue.getLength() < (120)) { //per la limitazione delle code
                txqueue.insert(pkt);

            } else {
                EV << "CODA  switch PIENA: DROPPING..." << endl;
                droppedFrameSwitch++;
                delete pkt;
            }
            emit(flrSwitch,(droppedFrameSwitch / totalFrameSwitch) * 100);
            emit(queue,txqueue.getLength());
            transmit();
            return;
        }

        EthFrame *frame = new EthFrame();

        ControlInfo *ci = check_and_cast<ControlInfo*>(
                msg->removeControlInfo());
        frame->setSrc(ci->getSrc());
        frame->setDst(ci->getDst());
        delete ci;

        Packet *app_pkt = check_and_cast<Packet*>(msg);
        int padding = 0;
        if (app_pkt->getByteLength() < 46) {
            padding = 46 - app_pkt->getByteLength();
        } else if (app_pkt->getByteLength() > 1500) {
            delete app_pkt;
            delete frame;
            EV << "AppPacket oversize: dropping..." << endl;
            return;
        }
        frame->addByteLength(padding);
        frame->encapsulate(app_pkt);
        frame->setSrcProcId(msg->getSrcProcId());

        //calcolo della deadline assoluta, deadline relativa + simTime()
        simtime_t DLA = app_pkt->getDeadlineR() + simTime();
        frame->setDeadlineA(DLA);
        bool insert = false;

        if (txqueue.getLength() == 120) { //limitazione delle code

            for (int i = 0; i < txqueue.getLength(); i++) {
                if (check_and_cast<EthFrame*>(txqueue.get(i))->getDeadlineA()
                        > DLA) {
                    txqueue.remove(txqueue.back());
                    insert = true;
                    return;
                }
            }
        }

        if (txqueue.getLength() < (120)) {
            for (int i = 0; i < txqueue.getLength(); i++) { //per l'algoritmo di EDF, si scorre la coda
                if (check_and_cast<EthFrame*>(txqueue.get(i))->getDeadlineA()
                        > DLA) { //se la deadline è inferiore si inserisce before
                    txqueue.insertBefore(txqueue.get(i), frame);
                    insert = true;
                    return;
                }
            }

            if (!insert)
                txqueue.insert(frame);

        } else {
            EV << "CODA FRAME PIENA: DROPPING..." << endl;
            droppedFrame[flusso]++;

            delete frame;
        }
        totalFrame[flusso]=tmp;
        transmit();
        emit(flr[flusso],
                (droppedFrame[flusso]
                        / totalFrame[flusso]) * 100);
        for(int i=0;i<vectorSize;i++){
            int coda=0;
            for(int i=0;i<txqueue.getLength();i++){
                if(txqueue.get(i)->getSrcProcId()==flusso){
                    coda++;
                }
            }
            emit(queueSig[flusso],coda);
        }
        emit(queue,txqueue.getLength());

        return;
    }

    //Se non è venuto dall'applicazione
    EthFrame *frame = check_and_cast<EthFrame*>(msg);
    double t = frame->getBitLength() / datarate;
    cMessage *rxtim = new cMessage("RxEndTimer");
    if (inSwitch) {
        rxtim->setControlInfo(frame);
        scheduleAt(simTime() + t, rxtim);
        return;
    }
    cPacket *app_pkt = frame->decapsulate();
    ControlInfo *ci = new ControlInfo();
    ci->setSrc(frame->getSrc());
    ci->setDst(frame->getDst());
    app_pkt->setControlInfo(ci);
    emit(deadlineSig, frame->getDeadlineA());

    delete frame;
    rxtim->setControlInfo(app_pkt);
    scheduleAt(simTime() + t, rxtim);

}

void EthNIC::transmit() {
    if (txstate == TX_STATE_IDLE) {
        if (!txqueue.isEmpty()) {
            cPacket *frame = txqueue.pop();
            send(frame, "channelOut");
            txstate = TX_STATE_TRANSMITTING;
            double t = frame->getBitLength() / datarate;
            cMessage *txtimer = new cMessage("TxEndTimer");
            scheduleAt(t + simTime(), txtimer);
        }
    }
}
